# RapidPass Multi-Module Project

This project implements RapidPass as a multi-module project that aggregates all required `pom.xml`
and makes each module available to each other.

## Sub Modules
### RapidPass API

The RapidPass API project implements the various API and services for the RapidPass Project.
#### Documentation
[Google Docs](https://docs.google.com/document/d/1U2Uf3jbUJalG6h6CJsrluM1tV5gfvf7S3tPQAicLnAw/edit#heading=h.ad6j117knp4n)
#### Repository
https://gitlab.com/dctx/rapidpass/rapidpass-api

### RapidPass Java Commons

The RapidPass Java Commons project provides shared libraries for common functionality in RapidPass Java projects.
#### Repository
https://gitlab.com/dctx/rapidpass/rapidpass-commons

## TODO
* Add `docker-compose`